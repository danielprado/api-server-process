<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Server extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'servers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'logged_user',
        'os_name',
        'cpu',
        'ip',
    ];

    /**
     * @return HasMany
     */
    public function processes()
    {
        return $this->hasMany(Process::class);
    }
}
