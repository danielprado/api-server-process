<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logged_user'   => 'required|string|max:191',
            'os_name'       => 'required|string|max:191',
            'cpu'           => 'required|string|max:191',
            'ip'            => 'required|ip',
            'process'       => 'required|array',
            'process.*.pid' => 'required|numeric',
            'process.*.username' => 'nullable|string|max:191',
            'process.*.name' => 'required|string|max:191',
            'process.*.vms' => 'required',
        ];
    }
}
