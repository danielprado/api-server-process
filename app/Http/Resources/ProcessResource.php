<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProcessResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'process_id'           => isset($this->id) ? (int) $this->id : null,
            'pid'                  => $this->pid ?? null,
            'username'             => $this->username ?? null,
            'name'                 => $this->name ?? null,
            'vms'                  => $this->vms ?? null,
            'server_id'            => isset($this->server_id) ? (int) $this->server_id : null,
            'created_at'           => isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'           => isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}
