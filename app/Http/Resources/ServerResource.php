<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $id = isset($this->id) ? (int) $this->id : null;
        return [
            'server_id'            => $id,
            'logged_user'          => $this->logged_user ?? null,
            'os_name'              => $this->os_name ?? null,
            'cpu'                  => $this->cpu ?? null,
            'ip'                   => $this->ip ?? null,
            'export_to_excel'      => route('service.export', ['server_id' => $id]),
            'process'              => ProcessResource::collection($this->processes),
            'created_at'           => isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'           => isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}
