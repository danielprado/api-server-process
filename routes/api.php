<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/services', [\App\Http\Controllers\ServiceController::class, 'index'])->name('service.index');
Route::post('/services', [\App\Http\Controllers\ServiceController::class, 'store'])->name('service.store');
Route::get('/services/export', [\App\Http\Controllers\ServiceController::class, 'excel'])->name('service.export');
